package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List< User > findByLastNameStartsWithIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.office.city = :city ORDER BY u.lastName ASC")
    List < User > findByCity(String city);

    List < User > findUserByExperienceGreaterThanEqual(int experience);

    @Query("SELECT u FROM User u WHERE u.office.city = :city AND u.team.room = :room ORDER BY u.lastName ASC")
    List < User > findByRoomAndCity(String city, String room, Sort sort);

    @Modifying
    @Transactional
    @Query("DELETE FROM User u WHERE u.experience < :experience")
    void deleteByExperience(int experience);

}
