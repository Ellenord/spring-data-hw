package com.bsa.springdata.user;

import com.bsa.springdata.project.Project;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable UUID id) {
        return userService.getUserById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
    }

    @PostMapping
    public UUID createUser(@RequestBody CreateUserDto user) {
        return userService.createUser(user)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create user."));
    }

    @GetMapping("/findByLastName")
    public List < UserDto > findByLastName(@RequestParam String lastName,
                                           @RequestParam(defaultValue = "0") int page,
                                           @RequestParam(defaultValue = "10") int size)
    {
        return userService.findByLastName(lastName, page, size);
    }

    @GetMapping("/findByCity")
    public List < UserDto > findByCity(@RequestParam String city)
    {
        return userService.findByCity(city);
    }

    @GetMapping("/findByExperience")
    public List < UserDto > findByExperience(@RequestParam int experience) {
        return userService.findByExperience(experience);
    }

    @GetMapping("/findByRoomAndCity/{city}/{room}")
    public List < UserDto > findByRoomAndCity(@PathVariable String city, @PathVariable String room) {
        return userService.findByRoomAndCity(city, room);
    }

    @DeleteMapping("/deleteByExperience/{experience}")
    public int deleteByExperience(@PathVariable int experience) {
        return userService.deleteByExperience(experience);
    }

}
