package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional< Team > findByName(String teamName);
    int countByTechnologyName(String newTechnology);
}
