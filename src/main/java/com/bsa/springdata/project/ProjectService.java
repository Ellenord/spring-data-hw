package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
//        return projectRepository
//                .findByTechnology(technology, PageRequest.of(0, 5))
//                .stream()
//                .map(ProjectDto::fromEntity)
//                .collect(Collectors.toList());
        return projectRepository
                .findAll()
                .stream()
                .filter(new Predicate<Project>() {
                    @Override
                    public boolean test(Project project) {
                        return project
                                .getTeams()
                                .stream()
                                .map(i -> i.getTechnology())
                                .collect(Collectors.toList())
                                .contains(technology);
                    }
                })
                .sorted(new Comparator<Project>() {
                    @Override
                    public int compare(Project a, Project b) {
                        var A = a.getTeams().stream().map(i -> i.getUsers().size()).reduce((x, y) -> x + y);
                        var B = b.getTeams().stream().map(i -> i.getUsers().size()).reduce((x, y) -> x + y);
                        return A.get().compareTo(B.get());
                    }
                })
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        return Optional.of(projectRepository
                .findAll()
                .stream()
                .sorted(new Comparator<Project>() {
                    @Override
                    public int compare(Project a, Project b) {
                        if (a.getTeams().size() != b.getTeams().size()) {
                            return Integer.valueOf(b.getTeams().size())
                                    .compareTo(Integer.valueOf(a.getTeams().size()));
                        }
                        var A = a.getTeams().stream().map(i -> i.getUsers().size()).reduce((x, y) -> x + y);
                        var B = b.getTeams().stream().map(i -> i.getUsers().size()).reduce((x, y) -> x + y);
                        if (A.get() != B.get()) {
                            return B.get().compareTo(A.get());
                        }
                        return b.getName().compareTo(a.getName());
                    }
                })
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList())
                .get(0));
    }

    public List<ProjectSummaryDto> getSummary() {
        return new ArrayList<>();
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
    }

    public int getCountWithRole(String role) {
        return -1;
        // TODO: Use a single query
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        return null;
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
    }
}
